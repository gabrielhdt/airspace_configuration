module Sset = Set.Make (struct
  type t = string

  let compare = compare
end)

module Smap = Map.Make (struct
  type t = string

  let compare = compare
end)

let argmax cmp xs =
  List.fold_left cmp (List.hd xs) xs

let pick lst =
  List.nth lst (Random.int (List.length lst))
