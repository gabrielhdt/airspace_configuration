module Sset : Set.S with type elt = string
module Smap : Map.S with type key = string

val argmax : ('a -> 'a -> 'a) -> 'a list -> 'a

val pick : 'a list -> 'a
(** [pick l] takes randomly an element from [l]. *)
